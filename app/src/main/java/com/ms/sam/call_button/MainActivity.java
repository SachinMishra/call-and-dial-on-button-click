package com.ms.sam.call_button;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

   private Button call_button,dial_button;
   private EditText et_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        call_button=findViewById(R.id.callID);
        dial_button=findViewById(R.id.dialID);

        et_phone=findViewById(R.id.contactID);

        dial_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String contact = et_phone.getText().toString();

                if (contact.equals(""))
                {
                    et_phone.setError("Please enter phone");
                } else
                    {

                    Intent intent = new Intent(Intent.ACTION_DIAL);

                    intent.setData(Uri.parse("tel:" + contact));
                    startActivity(intent);


                }

            }
        });

        call_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent=new Intent(Intent.ACTION_CALL);

                String contact=et_phone.getText().toString();

                if (contact.equals(""))
                {
                    et_phone.setError("Please enter phone");
                }

                else
                    {
                    intent.setData(Uri.parse("tel:" + contact));


                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.
                            permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(MainActivity.this, new String[]
                                {Manifest.permission.CALL_PHONE}, 0);

                        return;
                    }

                    startActivity(intent);

                }

            }
        });


    }
}
